package com.iteaj.iot.message;

import com.iteaj.iot.SocketMessage;

/**
 * 未解析报文体
 */
public abstract class UnParseBodyMessage extends SocketMessage {

    public UnParseBodyMessage(byte[] message) {
        super(message);
    }

    public UnParseBodyMessage(MessageHead head) {
        this(head, EMPTY_MESSAGE_BODY);
    }

    public UnParseBodyMessage(MessageHead head, MessageBody body) {
        super(head, body);
    }
}

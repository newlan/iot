package com.iteaj.iot.serial;

import com.fazecast.jSerialComm.SerialPort;
import com.iteaj.iot.client.ClientConnectProperties;

public class SerialConnectProperties extends ClientConnectProperties {

    private int safetySleepTime;
    private boolean useRS485Mode;
    private int sendDeviceQueueSize;
    private int receiveDeviceQueueSize;

    private int
            baudRate = 9600, // 波特率
            dataBits = 8, // 数据位
            parity = SerialPort.NO_PARITY, // 校验位
            stopBits = SerialPort.ONE_STOP_BIT; // 停止位

    public SerialConnectProperties(String com) {
        this(com, 100);
    }

    public SerialConnectProperties(String com, int safetySleepTime) {
        this(com, safetySleepTime, 4096, 4096);
    }

    public SerialConnectProperties(String com, int safetySleepTime, int sendDeviceQueueSize, int receiveDeviceQueueSize) {
        super(com, 0, com);
        this.setReaderIdleTime(3);
        this.setWriterIdleTime(3);
        this.safetySleepTime = safetySleepTime;
        this.sendDeviceQueueSize = sendDeviceQueueSize;
        this.receiveDeviceQueueSize = receiveDeviceQueueSize;
    }

    @Override
    public String toString() {
        return this.connectKey();
    }

    /**
     * 串口配置
     * @param dataBits 数据位
     * @param stopBits 停止位
     * @param parity 校验位
     * @return
     */
    public SerialConnectProperties config(int dataBits, int stopBits, int parity) {
        this.parity = parity;
        this.dataBits = dataBits;
        this.stopBits = stopBits;
        return this;
    }

    /**
     * 串口配置
     * @param baudRate 波特率
     * @param dataBits 数据位
     * @param stopBits 停止位
     * @param parity 校验位
     * @return
     */
    public SerialConnectProperties config(int baudRate, int dataBits, int stopBits, int parity) {
        this.parity = parity;
        this.dataBits = dataBits;
        this.baudRate = baudRate;
        this.stopBits = stopBits;
        return this;
    }

    /**
     * 是否用RS485模式
     * @return
     */
    public boolean isUseRS485Mode() {
        return useRS485Mode;
    }

    public SerialConnectProperties setUseRS485Mode(boolean useRS485Mode) {
        this.useRS485Mode = useRS485Mode; return this;
    }

    /**
     * 波特率
     * @return
     */
    public int getBaudRate() {
        return baudRate;
    }

    public SerialConnectProperties setBaudRate(int baudRate) {
        this.baudRate = baudRate; return this;
    }

    public SerialConnectProperties setDataBits(int dataBits) {
        this.dataBits = dataBits; return this;
    }

    public SerialConnectProperties setStopBits(int stopBits) {
        this.stopBits = stopBits; return this;
    }

    public SerialConnectProperties setParity(int parity) {
        this.parity = parity; return this;
    }

    /**
     * 数据位
     * @return
     */
    public int getDataBits() {
        return dataBits;
    }

    /**
     * 停止位
     * @return
     */
    public int getStopBits() {
        return stopBits;
    }

    /**
     * 校验位
     * @return
     */
    public int getParity() {
        return parity;
    }

    public int getSafetySleepTime() {
        return safetySleepTime;
    }

    public int getSendDeviceQueueSize() {
        return sendDeviceQueueSize;
    }

    public int getReceiveDeviceQueueSize() {
        return receiveDeviceQueueSize;
    }
}

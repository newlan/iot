package com.iteaj.iot.server.dtu.impl;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.dtu.DtuMessageAware;
import com.iteaj.iot.server.dtu.DtuDecoderServerComponent;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

/**
 * 通用的dtu服务组件
 */
public class CommonDtuServerComponent extends DtuDecoderServerComponent<CommonDtuServerMessage> {

    public CommonDtuServerComponent(ConnectProperties connectProperties) {
        this(connectProperties, new CommonDtuMessageAware());
    }

    public CommonDtuServerComponent(ConnectProperties connectProperties, DtuMessageAware<CommonDtuServerMessage> dtuMessageAware) {
        super(connectProperties, dtuMessageAware);
    }

    @Override
    public String getDesc() {
        return "支持DTU+任意设备协议";
    }

    @Override
    public String getName() {
        return "通用DTU服务";
    }

    @Override
    public AbstractProtocol doGetProtocol(CommonDtuServerMessage message) {
        return remove(message.getHead().getMessageId());
    }

    @Override
    protected ClientInitiativeProtocol<CommonDtuServerMessage> doGetProtocol(CommonDtuServerMessage message, ProtocolType type) {
        return null;
    }
}

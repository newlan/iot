create table t_collect_entity
(
    id     bigint auto_increment
        primary key,
    i      double default 0 null,
    v      decimal(8, 2)    not null comment '0.00',
    ts     datetime         null comment '采集时间',
    power1 double           not null comment '功率1',
    power2 double           null comment '功率2',
    py     double           null comment '功率因子'
)
    comment '数据采集实体';

create table t_collect_map
(
    id     bigint auto_increment
        primary key,
    i      double default 0 null,
    v      decimal(8, 2)    not null comment '0.00',
    ts     datetime         null comment '采集时间',
    power1 double           not null comment '功率1',
    power2 double           null comment '功率2',
    py     double           null comment '功率因子'
)
    comment '数据采集map';

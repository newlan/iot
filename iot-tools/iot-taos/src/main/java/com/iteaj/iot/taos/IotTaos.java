package com.iteaj.iot.taos;

import javax.sql.DataSource;

/**
 * Taos物联网分布式数据库适配
 */
public interface IotTaos {

    DefaultTaosDatabaseSource databaseSource = new DefaultTaosDatabaseSource();

    /**
     * 使用的jdbc模板, 用来支持多Taos数据源
     * @return jdbcTemplate
     */
    default DataSource taosDataSource(Object entity) {
        return databaseSource.taosDataSource;
    }

    /**
     * 默认的taos jdbc数据源
     */
    class DefaultTaosDatabaseSource {
        public DataSource taosDataSource;
    }
}
